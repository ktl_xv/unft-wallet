# uNFT Wallet

>️️️️⚠️
>
> This repository is deprecated. Development of uNFTWallet has been transferred to https://github.com/poap-xyz/uNFT-Wallet.
>
> The last version of uNFTWallet built from this repository can be found at ipfs://unftwallet.ktlxv.eth (ipfs://bafybeibyjhdg6jux6fgpstilhkku6mhdy53fbpcmbx26zhvhqkdo3o7dey)
>
>⚠️

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npm run dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
npm run build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
